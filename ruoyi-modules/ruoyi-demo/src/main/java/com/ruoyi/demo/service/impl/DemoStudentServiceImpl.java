package com.ruoyi.demo.service.impl;

import java.util.List;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import com.ruoyi.demo.mapper.DemoStudentMapper;
import com.ruoyi.demo.domain.DemoStudent;
import com.ruoyi.demo.service.IDemoStudentService;

/**
 * 学生信息单表(mb)Service业务层处理
 * 
 * @author 数据小王子
 * 2023-07-11
 */
@Service
public class DemoStudentServiceImpl implements IDemoStudentService 
{
    @Resource
    private DemoStudentMapper demoStudentMapper;

    /**
     * 查询学生信息单表(mb)
     * 
     * @param studentId 学生信息单表(mb)主键
     * @return 学生信息单表(mb)
     */
    @Override
    public DemoStudent selectDemoStudentByStudentId(Long studentId)
    {
        return demoStudentMapper.selectDemoStudentByStudentId(studentId);
    }

    /**
     * 查询学生信息单表(mb)列表
     * 
     * @param demoStudent 学生信息单表(mb)
     * @return 学生信息单表(mb)
     */
    @Override
    public List<DemoStudent> selectDemoStudentList(DemoStudent demoStudent)
    {
        return demoStudentMapper.selectDemoStudentList(demoStudent);
    }

    /**
     * 新增学生信息单表(mb)
     * 
     * @param demoStudent 学生信息单表(mb)
     * @return 结果
     */
    @Override
    public int insertDemoStudent(DemoStudent demoStudent)
    {
        return demoStudentMapper.insertDemoStudent(demoStudent);
    }

    /**
     * 修改学生信息单表(mb)
     * 
     * @param demoStudent 学生信息单表(mb)
     * @return 结果
     */
    @Override
    public int updateDemoStudent(DemoStudent demoStudent)
    {
        return demoStudentMapper.updateDemoStudent(demoStudent);
    }

    /**
     * 批量删除学生信息单表(mb)
     * 
     * @param studentIds 需要删除的学生信息单表(mb)主键
     * @return 结果
     */
    @Override
    public int deleteDemoStudentByStudentIds(Long[] studentIds)
    {
        return demoStudentMapper.deleteDemoStudentByStudentIds(studentIds);
    }

    /**
     * 删除学生信息单表(mb)信息
     * 
     * @param studentId 学生信息单表(mb)主键
     * @return 结果
     */
    @Override
    public int deleteDemoStudentByStudentId(Long studentId)
    {
        return demoStudentMapper.deleteDemoStudentByStudentId(studentId);
    }
}
